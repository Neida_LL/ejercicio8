import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  n:number=0;
  nro:number=0;
  mensaje!:string;
  constructor() { 
  }

  ngOnInit(): void {
  }
  
  sumar():void{
    this.n++<50 ? this.n: this.n=50;
  }
  restar():void{
    this.n-->0? this.n: this.n=0;
  }
 
  darMensaje():string{ 
      return this.n>0 && this.n<50?this.n+'':'El valor no puede estar fuera de los limites';
    }

}
